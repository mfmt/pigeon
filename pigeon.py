#!/usr/bin/env python3

import os
import re
import sys
import time
import gzip
import datetime
import json
import pickle
import asyncio
import shutil
import subprocess
import random
import argparse
import pathlib
from apt import Cache
import yaml
import psutil

# Define our priorities.
P_CRITICAL = "critical"
P_WARNING = "warning"
P_INFO = "info"
P_HEARTBEAT = "heartbeat"
P_DEBUG = "debug"

# Save our hostname
HOSTNAME = os.uname()[1]

# STATE_KEYS ensure that every new alert gets a unique key
# which, in turn, ensures that existing alerts respect the
# elastalert realert time frame (so we don't get repeated
# realerts) but a new alert gets a new alert.
STATE_KEYS = {}

# Set debug: true in config file for more verbose output.
DEBUG = False

def log(msg, priority, key=None):
    if priority == P_DEBUG and not DEBUG:
        # Only print debug messages if we have debug configured.
        return

    # Get the name of the method that called us.
    method = sys._getframe(1).f_code.co_name
    allowed_priorities = [
        P_CRITICAL,
        P_WARNING,
        P_INFO,
        P_HEARTBEAT,
        P_DEBUG
    ]
    if priority not in allowed_priorities:
        orig_msg = msg
        orig_priority = priority
        msg = f"Unknown priority ({orig_priority}, original message was: {orig_msg})"
        priority = P_WARNING

    # Optionally, a key can be provided which will be used to generate a state
    # key. The state key ensures that all alerts with the same key will have
    # the same state key so elastalert knows it's the same alert and won't
    # realert every time we send it. State keys are cleared when an alert is
    # cleared.
    state_key = ""
    if key:
        formatted_key = get_state_key(method, key)
        state_key = f"-{formatted_key}"

    formatted_msg = (f"pigeon-query-key: {HOSTNAME}-{method}-{priority}{state_key}, "
        f"pigeon-method: {method}, pigeon-priority: {priority}, pigeon-msg: {msg}")
    print (formatted_msg, flush=True)

def get_state_key(method, key):
    if not method in STATE_KEYS:
        STATE_KEYS[method] = {}
    if not key in STATE_KEYS[method]:
        alpha = 'abcdefghijklmnopqrstuvwxyz'
        STATE_KEYS[method][key] = ''.join(random.choice(alpha) for i in range(10))
    return STATE_KEYS[method][key]

def clear_state_keys(method):
    if DEBUG:
        log(f"The method {method} returned a healthy status.", P_DEBUG)
    if method in STATE_KEYS:
        STATE_KEYS[method] = {}

async def heartbeat():
    while True:
        # Once an hour print out an ok message so we can find hosts that are
        # not working properly.
        log("thump thump", P_HEARTBEAT)
        await asyncio.sleep(60 * 5)

async def backupninja_loop(arg={}):
    while True:
        if backupninja(arg):
            clear_state_keys("backupninja")

        await asyncio.sleep(arg["interval"] * 60)

def backupninja(arg={}):
    path = "/var/log/backupninja.log"
    healthy = True
    if not os.path.exists(path):
        # It's possible that backupninja simply has not yet run yet, so
        # we'll consider this a warning which will get resolved in a day.
        log("No backupninja log file. Maybe it has not yet run?", P_WARNING)
        return False

    finished_line = None
    mysqldump_failure = False
    lock_failure = False
    with open(path, "r") as file:
        finished_line_re = re.compile(r"(.*) Info: FINISHED: .* ([0-9]+) fatal. ([0-9]+) error. ([0-9]+) warning.")
        mysqldump_failure_line_re = re.compile(r"(.*) Warning: Failed to dump mysql databases")
        lock_failure_line_re = re.compile(r"(.*) Failed to create/acquire the lock")
        for line in file:
            # We iterate over all of the lines in the file - but we only
            # care about lines indicating the FINISHED status or a serious error.
            is_finished_line = finished_line_re.match(line)
            if not is_finished_line:
                if mysqldump_failure_line_re.match(line):
                    mysqldump_failure = True
                if lock_failure_line_re.match(line):
                    lock_failure = True

                continue
            # We overwrite the finished_line variable each time we find one to ensure we
            # end up with the most recent.
            finished_line = is_finished_line
    if not finished_line:
        # There are some legit reasons for a lack of a finished line - for
        # example, logrotate might rotate the file or it's a new file and
        # backupninja is still running.

        # First, we see if there is a rotated one.
        rotated_path = "/var/log/backupninja.log.1.gz"
        if os.path.exists(rotated_path):
            with gzip.open(rotated_path, "r") as file:
                for line in file:
                    line = line.decode('utf-8')
                    is_finished_line = finished_line_re.match(line)
                    if not is_finished_line:
                        # Skip it. We don't care.
                        continue
                    finished_line = is_finished_line
            if not finished_line:
                log("No finished line. Has backupninja run?", P_WARNING)
                return False
        else:
            # We assume it's running for the first time. Since there is no
            # rush to get a backup failed warning, we are going to suppress
            # this error for 12 hours after the modification date of the
            # backupninja file.
            file_time = os.path.getmtime(path)
            current_time = time.time()
            diff_time = current_time - file_time
            if diff_time > 12 * 60 * 60:
                log("No finished line. Has backupninja run?", P_WARNING)
                return False
            return True

    log_date = finished_line.group(1)
    fatal = int(finished_line.group(2))
    error = int(finished_line.group(3))
    warning = int(finished_line.group(4))

    # Check the date to make sure it's not too old.

    # Bah. Backupninja prints the date without the year (Dec 05
    # 05:23:12).
    # So, we get the year from the log modification date
    year = datetime.datetime.fromtimestamp(os.path.getmtime(path)).year
    log_date = f"{year} {log_date}"
    log_date = datetime.datetime.strptime(log_date, "%Y %b %d %H:%M:%S")

    # Check if the last run is older then 2 days.
    delta = datetime.timedelta(days=2)

    now = datetime.datetime.now()
    if log_date < now - delta:
        log(f"Backupninja's last run is more then 2 days ago: {log_date}.", P_WARNING)
        healthy = False

    # Now check if there are any errors.
    if fatal > 0:
        # Enough said. This will generate a warning that should be investigated.
        log("Backupninja's last run had a failure.", P_WARNING)
        healthy = False
    if error > 0 or warning > 0:
        # These typically can be ignored (like file changed while backing up).
        # But sometimes it is more serious.
        if error > 0:
            log("Backupninja's last run had an error.", P_INFO)
            healthy = False
        if warning > 0:
            log("Backupninja's last run had a warning.", P_INFO)
            healthy = False

        # Now check if we need to escale an INFO. These serious problems are labeled
        # by backupninja as just warnings and they should be escalated.
        if mysqldump_failure:
            log("Backupninja's last run may have failed a database dump.", P_WARNING)
            healthy = False
        if lock_failure:
            log("Backupninja's last run may have had a lock file failure.", P_WARNING)
            healthy = False

    if healthy:
        log("Backupninja log shows 0 errors.", P_DEBUG)

    return healthy

async def disk_usage_loop(arg={}):
    while True:
        if disk_usage(arg):
            clear_state_keys("disk_usage")

        await asyncio.sleep(arg["interval"] * 60)

def disk_usage(arg={}):
    healthy = True
    for check in arg["checks"]:
        paths = []
        percent = check["percent"]
        priority = check["priority"]
        if "path" in check:
            paths = [ check["path"] ]
        if not paths:
            for mount in psutil.disk_partitions():
                mountpoint = mount[1]
                paths.append(mountpoint)
        for path in paths:
            try:
                usage = shutil.disk_usage(path)
            except FileNotFoundError:
                # On systems running docker, file systems appear and disappear quickly
                # so by the time we check them, they might be gone.
                print(f"Failed to record disk usage on {path}")
            total = usage[0]
            used = usage[1]
            used_percent = (used + .05 * total) / total * 100
            if used_percent > percent:
                healthy = False
                msg = (f"{path} disk usage is {used_percent:.2g} percent, "
                    f"which is over the allowed {percent} percent.")
                log(msg, priority, path)
            else:
                msg = (f"{path} disk usage is {used_percent:.2g} percent, "
                    f"which is under the allowed {percent} percent.")
                log(msg, P_DEBUG)

    return healthy

async def oomkiller_loop(arg={}):
    while True:
        if oomkiller(arg):
            clear_state_keys("oomkiller")

        await asyncio.sleep(arg["interval"] * 60)

def oomkiller(arg={}):
    healthy = True
    journalctl = "/usr/bin/journalctl"
    if not os.path.exists(journalctl):
        # Don't bother testing if this location works. If systemctl is in
        # neither location, we should throw an excdeption.
        journalctl = "/bin/journalctl"

    # Designed to be checked every 10 minutes. Go 11 minutes back just to be
    # sure we don't miss one at the risk of getting notified twice.
    proc = subprocess.run(
        [journalctl, "-k",  "--since", "11 minutes ago"],
        capture_output=True,
        text=True
    )
    output_lines = proc.stdout.split('\n')
    if proc.returncode != 0:
        log(
            (f"The journalctl command to show oomkiller returned a non-zero code "
            f"and error: {proc.stderr}."),
            P_CRITICAL
        )
        healthy = False
    else:
        for line in output_lines:
            # This might be "Out of memory: Kill" or "out of memory: Kill"
            # (if it's a cgroup)
            if "memory: Kill" in line:
                healthy = False
                log(f"Oomkiller found via journalctl: {line}", P_CRITICAL)

    return healthy

async def systemd_loop(arg={}):
    while True:
        if systemd(arg):
            clear_state_keys("systemd")

        await asyncio.sleep(arg["interval"] * 60)

def systemd(arg={}):
    healthy = True
    # We seem to have some servers without /usr/bin/systemctl. Why?
    systemctl = "/usr/bin/systemctl"
    if not os.path.exists(systemctl):
        # Don't bother testing if this location works. If systemctl is in
        # neither location, we should throw an excdeption.
        systemctl = "/bin/systemctl"

    proc = subprocess.run([systemctl, "show", "--property", "SystemState"], capture_output=True, text=True)
    state = proc.stdout.strip()
    if proc.returncode != 0:
        log(f"The systemctl show command returned a non-zero code and error: {proc.stderr}.", P_CRITICAL)
        healthy = False
    elif state != "SystemState=running" and state != "SystemState=starting":
        # Let's pop off the first unit that is listed.
        proc = subprocess.run([systemctl, "--failed"], capture_output=True, text=True)
        failed = proc.stdout.split("\n")
        if proc.returncode != 0:
            log(f"The systemctl --failed command returned a non-zero code and error: {proc.stderr}.",
                P_CRITICAL)
        else:
            if len(failed) > 1:
                first_failed = failed[1]
            else:
                first_failed = failed[0]
            # A systemd service failure is typicall a critical event.
            priority = P_CRITICAL
            # However, certbot warnings happen a full month before
            # expiration, # these can be handled via email.
            if "certbot" in first_failed:
                priority = P_WARNING
            log(f"Systemd state is NOT set to 'running': {state}. First failed unit: {first_failed}.",
                priority, first_failed)
        healthy = False
    else:
        log("Systemd state is set to 'running'.", P_DEBUG)

    return healthy

async def ntpsync_loop(arg={}):
    while True:
        if ntpsync(arg):
            clear_state_keys("ntpsync")

        await asyncio.sleep(arg["interval"] * 60)

def ntpsync(arg={}):
    healthy = True
    proc = subprocess.run(
        ["timedatectl", "show", "--property", "NTPSynchronized"],
        capture_output=True,
        text=True)
    # NTP Sync is critical because if it goes off sync, we may stop getting alerts.
    state = proc.stdout.strip()
    if proc.returncode != 0:
        log(("The timedatectl command returned a non-zero code "
            f"and the message: {proc.stderr}."), P_CRITICAL)
        healthy = False
    elif state != "NTPSynchronized=yes":
        log(("Clock is not sychronized. Test with: timedatectl. "
            "Check /etc/systemd/timesyncd.conf. Fix with: "
            "systemctl restart systemd-timesyncd.service."), P_CRITICAL)
        healthy = False
    else:
        log("Clock is sychronized.", P_DEBUG)

    return healthy

async def mailq_loop(arg={}):
    while True:
        if mailq(arg):
            clear_state_keys("mailq")

        await asyncio.sleep(arg["interval"] * 60)

def mailq(arg={}):
    healthy = True
    # Keep track of paths so we don't count a path twice.
    paths = {}
    for check in arg["checks"]:
        path = check["path"]
        allowed_count = check["count"]
        priority = check["priority"]
        if not path in paths:
            # Have the `find` command print a dot for each file it finds.
            proc = subprocess.run(["find", f"{path}/defer", "-type", "f", "-printf", "."], capture_output=True, text=True)
            if proc.returncode != 0:
                log(
                    (f"Failed to run find command to detect mailq limit, "
                    f"return code {proc.returncode} and error: {proc.stderr}."),
                    P_CRITICAL,
                    path
                )
                healthy = False
            else:
                defer = proc.stdout
                proc = subprocess.run(
                    ["find", f"{path}/active", "-type", "f", "-printf", "."],
                    capture_output=True,
                    text=True
                )
                if proc.returncode != 0:
                    log(
                        (f"Failed to run find command to detect mailq limit, "
                        f"return code {proc.returncode} and error: {proc.stderr}."),
                        P_CRITICAL,
                        path
                    )
                    healthy = False
                else:
                    active = proc.stdout
                    actual_count = len(defer) + len(active)
                    paths[path] = actual_count
                    if actual_count > allowed_count:
                        log(
                            (f"mailq limit of {allowed_count} is exceeded for path: "
                            f"{path}. Total messages in queue: {actual_count}"),
                            priority,
                            path
                        )
                        healthy = False
                    else:
                        log(
                            (f"mailq limit of {allowed_count} is not exceeded for path: "
                            f"{path}. Total messages in queue: {actual_count}"),
                            P_DEBUG
                        )

    return healthy

async def smartctl_loop(arg={}):
    while True:
        if smartctl(arg):
            clear_state_keys("smartctl")

        await asyncio.sleep(arg["interval"] * 60)

def smartctl(arg={}):
    # See https://www.extremetech.com/computing/194059-using-smart-to-accurately-predict-when-a-hard-drive-is-about-to-die
    # We are paying attention to:
    # SMART ID 5 (0x05): Relocated Sectors Count
    # SMART ID 187 (0xBB): Reported Uncorrectable Errors
    # SMART ID 188 (0xBC): Command Timeout
    # SMART ID 197 (0xC5): Current Pending Sector Count
    # SMART ID 198 (0xC6): Uncorrectable Sector Count
    #
    # And we expect their counts to be 0 (can be overridden in the conf file).

    expr = re.compile(r'^(  5|187|188|197|198) ([a-zA-Z_]+) .*? ([0-9]+)$')
    healthy = True
    for check in arg["checks"]:
        path = check['path']
        allowed_count = 0
        proc = subprocess.run(["/usr/sbin/smartctl", "-A", path], capture_output=True, text=True)
        if proc.returncode != 0:
            log(f"Device {path} returned a non-zero return code and error: {proc.stderr}.", P_WARNING, path)
            healthy = False
        else:
            for line in proc.stdout.split("\n"):
                result = expr.match(line)
                if result:
                    attribute_id = int(result.group(1).strip())
                    attribute = result.group(2)
                    actual_count = int(result.group(3))

                    # By default we expect the count to be 0, but it can be overridden if you have a
                    # higher number and you just want to be alerted if it goes up.
                    if "count_exceptions" in check and attribute_id in check["count_exceptions"]:
                        allowed_count = check["count_exceptions"][attribute_id]
                    else:
                        allowed_count = 0

                    if actual_count > allowed_count:
                        log(
                            (f"Device {path} has {actual_count} count for smartctl attribute {attribute} "
                            f"with attribute id {attribute_id}. The thresshold is {allowed_count}."),
                            P_WARNING,
                            "{path}-{attribute}"
                        )
                        healthy = False
                    else:
                        log(
                            (f"Device {path} has {actual_count} count for smartctl attribute {attribute} "
                            f"with attribure id {attribute_id}. The thresshold is {allowed_count}."),
                            P_DEBUG
                        )

    return healthy

async def filter_check_loop(arg={}):
    while True:
        if filter_check(arg):
            clear_state_keys("filter_check")

        await asyncio.sleep(arg["interval"] * 60)

# Filter check sends email via our various relay IPs to various commercial
# providers and then checks to make sure the email lands in the inbox and not
# the spam box. Each run we send a message, then wait for the next run to
# see if the message is in the right box, that ensures that we don't get any
# errors due to time spent in the message queue. And, we stagger sends and
# vary senders, subjects and bodies to avoid looking like a spammer.
def filter_check(arg={}):
    healthy = True
    sendtos = arg["sendtos"]
    state_path = "/run/pigeon.filter_check.pck"

    state = None
    if os.path.exists(state_path):
        # Now, run. Load our pairs of relay IPs/providers.
        with open(state_path, 'rb') as handle:
            state = pickle.load(handle)
            log(f"Loaded pickle file, run count: {state['runs']}.", P_DEBUG)
            # After running through each pair, reset the file (this way we are sure to get
            # any new postfix instances that may have been added.)
            if state["runs"] >= len(state["pairs"]):
                state = None

    if not state:
        # Collect a list of all the postfix instances running and then parse their IP address
        # from the main.cf file to build a list of pairs to send to.
        cmd = [ "systemctl", "list-units", "--state=running", "--output=json", "postfix@postfix-*" ]
        instances = subprocess.run(cmd, capture_output=True, text=True)
        if instances.returncode != 0:
            log(f"Failed to collect list of running postfix instances: {instances.stderr}.", P_WARNING)
            healthy = False
        else:
            # unit will be something like postfix@postfix-priority002.service.
            postfix_instance_expr = re.compile(r'postfix@postfix-([a-z]+[0-9]+)\.service')
            ip_addr_expr = re.compile(r'^inet_interfaces = ([0-9.]+)')
            ips = []
            for instance in json.loads(instances.stdout):
                log(f"Found postfix instance: {instance}.", P_DEBUG)
                postfix_instance_match = postfix_instance_expr.match(instance["unit"])
                if postfix_instance_match:
                    instance_name = postfix_instance_match.group(1)
                    main_path = f"/etc/postfix-{instance_name}/main.cf"
                    if os.path.exists(main_path):
                        with open(main_path) as file:
                            lines = file.readlines()
                            for line in lines:
                                ip_addr_match = ip_addr_expr.match(line)
                                if ip_addr_match:
                                    ip = ip_addr_match.group(1)
                                    log(f"Found IP address: {ip}.", P_DEBUG)
                                    ips.append(ip)

            # Create pairs of sender and receivers
            state = {
                "runs": 0,
                "pairs": {}
            }
            for sendto in sendtos:
                for ip in ips:
                    state["pairs"][sendto + ":" + ip] = {"sendto": sendto, "sendvia": ip}
            # Write out our pairs
            with open(state_path, 'wb') as handle:
                pickle.dump(state, handle, protocol=pickle.HIGHEST_PROTOCOL)



    # Fetch any message with a msgid to see if it's in the right place.
    for key in state["pairs"]:
        if "msgid" in state["pairs"][key] and state["pairs"][key]["msgid"]:
            # We have a message id, so check to see if we are in the spam or inbox.
            cmd = [
                "/usr/local/sbin/filter-check",
                "--fetchfrom",
                state["pairs"][key]['sendto'],
                "--messageid",
                state["pairs"][key]['msgid']
            ]
            ps = subprocess.run(cmd)
            if ps.returncode != 0:
                if ps.returncode == 1:
                    problem = "Message landed in spam box."
                elif ps.returncode == 2:
                    problem = "Message did not arrive."
                else:
                    problem = f"Unknown message - return code was {ps.returncode}."
                log(
                    (f"Problem: {problem} Happened while fetching message id "
                    f"{state['pairs'][key]['msgid']} from "
                    f"{state['pairs'][key]['sendto']} via "
                    f"{state['pairs'][key]['sendvia']}"),
                    P_WARNING,
                    key
                )
                healthy = False
            else:
                log(
                    (f"Fetched message id {state['pairs'][key]['msgid']} "
                    f"from {state['pairs'][key]['sendto']} via "
                    f"{state['pairs'][key]['sendvia']}"),
                    P_DEBUG
                )

            # Remove the msgid from this pair so we won't try to retreive this message again.
            state["pairs"][key].pop("msgid", None)

    # We can't send to all pairs at the same time or we'll look like a spammer.
    # So instead, we only send one message per run. The number of times we have
    # run acts as an index indicating which pair is next.
    index = 0
    for key in state["pairs"]:
        if index == state["runs"]:
            msg = random.choice(arg["msgs"])
            emailfrom = random.choice(arg["emailfroms"])
            subject = random.choice(arg["subjects"])
            cmd = [
                "/usr/local/sbin/filter-check",
                "--sendonly",
                "--quiet",
                "--sendvia",
                state["pairs"][key]['sendvia'],
                "--sendto",
                state["pairs"][key]['sendto'],
                "--msg",
                msg,
                "--subject",
                subject,
                "--emailfrom",
                emailfrom
            ]
            ps = subprocess.run(cmd, capture_output=True, text=True)
            if ps.returncode != 0:
                log(
                    (f"Exit code {ps.returncode} sending message from "
                    f"{state['pairs'][key]['sendto']} via "
                    f"{state['pairs'][key]['sendvia']}."),
                    P_WARNING,
                    key
                )
                healthy = False
            else:
                msgid = str(ps.stdout).strip()
                log(
                    (f"Sent message with message id {msgid} from "
                    f"{state['pairs'][key]['sendto']} via "
                    f"{state['pairs'][key]['sendvia']} from "
                    f"{emailfrom} with subject {subject} and message "
                    f"starting with '{msg[0:15]}...'"),
                    P_DEBUG
                )
                state["pairs"][key]["msgid"] = msgid
            break
        index = index + 1

    # Write out our new paris with the new message ids.
    state["runs"] = state["runs"] + 1
    with open(state_path, 'wb') as handle:
        pickle.dump(state, handle, protocol=pickle.HIGHEST_PROTOCOL)

    return healthy

async def mdadm_loop(arg={}):
    while True:
        if mdadm(arg):
            clear_state_keys("mdadm")

        await asyncio.sleep(arg["interval"] * 60)

def mdadm(arg={}):
    healthy = True
    path = "/proc/mdstat"
    # We match a line like: md1 : active raid1 sdc3[0] sdd3[1]
    # to capcture md1.
    raid_name_expr = re.compile(r'(md[0-9]+) :')
    # We match a line like: 3906281280 blocks super 1.2 [2/2] [UU]
    # to capture 2 and 2.
    raid_health_expr = re.compile(r'\s\[([0-9]+)/([0-9]+)\]\s')
    with open(path, "r") as file:
        for line in file:
            line = line.strip()
            raid_name_match = raid_name_expr.match(line)
            if raid_name_match:
                raid_name = raid_name_match.group(1)
                log(f"Found raid array named '{raid_name}' on line {line}", P_DEBUG)
                continue
            raid_health_match = raid_health_expr.search(line)
            if raid_health_match:
                # We have a match to analyze.
                expected_members = raid_health_match.group(1)
                actual_members = raid_health_match.group(2)
                log(
                    f"Found health ratio: '[{expected_members}/{actual_members}]' on line {line}",
                    P_DEBUG
                )
                if not expected_members == actual_members:
                    log(
                        (f"Raid device {raid_name} is in a failed state. /proc/mdstat reports: "
                        f"[{expected_members}/{actual_members}]"),
                        P_WARNING,
                        raid_name
                    )
                    healthy = False
                raid_name = None

    return healthy

async def apt_loop(arg={}):
    while True:
        if apt(arg):
            clear_state_keys("apt")

        await asyncio.sleep(arg["interval"] * 60)

def apt(arg={}):
    # The goal is to raise an alert if there have been pending package updates
    # for longer then a month (the default). We track using a file. If there
    # are no package updates, we update the file's modification time with the
    # current date. If there are pending package updates, we check the
    # modification time of the file and raise an alert if it's older then the
    # allowed threshhold.
    healthy = True
    path = "/var/log/mayfirst.apt.pending"
    pending_file = pathlib.Path(path)
    if not pending_file.exists():
        pending_file.touch()
        # This is the first time we have run, there is nothing to check.
        log("First run, created mayfirst.apt.pending file.", P_DEBUG)

    else:
        # Check for pending updates.
        log("mayfirst.apt.pending file exists.", P_DEBUG)
        # Build cache of all Debian packages. Note: we update the cache daily on a
        # cron job so we don't bother doing that here.
        cache = Cache()
        for package in cache:
            # Ensure package is installed.
            if hasattr(cache[package].installed, 'record' ):
                # Check if the candidate is a different version then the installed package.
                if cache[package].candidate.record["Version"] != cache[package].installed.record["Version"]:
                    # As long as just one package needs to be upgraded...
                    log(f"There is at least one package to upgrade: {package}.", P_DEBUG)
                    # Check modification time of our pending file.
                    pending_file_time = datetime.datetime.fromtimestamp(pending_file.stat().st_mtime)
                    now_time = datetime.datetime.now()
                    # Check if the last run is older then threshhold days.
                    delta = datetime.timedelta(days=arg['threshhold'])
                    if now_time > pending_file_time + delta:
                        log(f"More then {arg['threshhold']} days have passed since last upgrade.", P_WARNING)
                        healthy = False
                    else:
                        log(f"Less then {arg['threshhold']} days have passed since last upgrade, not alerting.", P_DEBUG)
                    break
    if healthy:
        # We have no packages to upgrade. Hoory. Update the pending_file time.
        log("No pending package updates.", P_DEBUG)
        pending_file.touch()

    return healthy

async def mistert_audit_relayers_loop(arg={}):
    while True:
        if mistert_audit_relayers(arg):
            clear_state_keys("mistert_audit_relayers")

        await asyncio.sleep(arg["interval"] * 60)

def mistert_audit_relayers(arg={}):
    cmd = ["mistert", "--log-level", "error", "--since", f"{arg['interval']}m",  "alert", "--host-type", "cf"]
    proc = subprocess.run(cmd, capture_output=True, text=True)
    healthy = True
    if proc.returncode != 0:
        out = proc.stdout.strip()
        log(f"Possible compromised email relayers: {out}", P_CRITICAL)
        healthy = False
    else:
        log("No relayers need to be audited.", P_DEBUG)

    return healthy

async def mysql_max_connections_loop(arg={}):
    while True:
        if mysql_max_connections(arg):
            clear_state_keys("mysql_max_connections")

        await asyncio.sleep(arg["interval"] * 60)

def mysql_max_connections(arg={}):
    healthy = True
    cmd = ["mysql", "--skip-column-names", "--execute", "SELECT (@@max_connections - (SELECT COUNT(*) FROM information_schema.processlist))"]
    proc = subprocess.run(cmd, capture_output=True, text=True)
    if proc.returncode != 0:
        out = proc.stdout.strip()
        log(f"Failed to connect to MariaDB server: {out}", P_CRITICAL)
        healthy = False
    else:
        count = int(proc.stdout)
        if count < 10:
            log(f"Only {count} available connections available on MariaDB server. Raise max_connections value.", P_CRITICAL)
        else:
            log(f"There are {count} available MariaDB connections. That should be enough.", P_DEBUG)

    return healthy

def parse_config():
    config  = None
    # Take a yaml file in the current working directory first, and if it
    # doesn't exist, try for a system-wide location.
    conf_paths = [ "pigeon.yml", "/etc/pigeon.yml" ]
    for conf_path in conf_paths:
        if os.path.isfile(conf_path):
            with open(conf_path, 'r') as file:
                conf_data = file.read(10000)
            config = yaml.safe_load(conf_data)
            break
    if not config:
        raise RuntimeError("Failed to find a configuration file.")

    return config

# Ensure that we quit if we encounter any exceptions whatsover
# so we can detect that the program is not running and fix it.
def exception_handler(badloop, context):
    badloop.stop()
    raise context["exception"]

# Main program logic begins.

# Parse the command line options.
parser = argparse.ArgumentParser(description='Detect and report problems on the host.')
parser.add_argument('--debug', help='Print results of all tests, even when they pass', default=False, action='store_true')
parser.add_argument('--method', help='Limit to a particular check to run, repeat as needed', action='append' )
args = parser.parse_args()

if args.debug:
    DEBUG = True

# Parse the configuration file.
conf = parse_config()

# Check command line for methods to add.
if args.method:
    # We only run the checks specified on the command line. And we run them even
    # if they are not enabled in the conf file.
    methods = args.method
else:
    # We only run the checks specified in the configuration file that are enabled.
    methods = []
    for m in conf:
        if conf[m]["enabled"] is True:
            methods.append(m)

# In debug mode we don't run in async code. Instead we just run each test once
# and stop. In debug mode, we run via async forever.
if not DEBUG:
    # Setup the async run.
    loop = asyncio.get_event_loop()
    loop.set_exception_handler(exception_handler)

    # Add the hearbeat once an hour.
    loop.create_task(heartbeat())

for method_index in methods:
    method_args = conf[method_index]
    # By default, the index is the method name, but it's also possible to
    # specify using the method key so you can call the same method twice
    # with different arguments.
    if "method" in conf[method_index]:
        m = conf[method_index]["method"]
    else:
        m = method_index
    if m == "backupninja":
        if DEBUG:
            backupninja(method_args)
        else:
            loop.create_task(backupninja_loop(method_args))
    elif m == "disk_usage":
        if DEBUG:
            disk_usage(method_args)
        else:
            loop.create_task(disk_usage_loop(method_args))
    elif m == "systemd":
        if DEBUG:
            systemd(method_args)
        else:
            loop.create_task(systemd_loop(method_args))
    elif m == "ntpsync":
        if DEBUG:
            ntpsync(method_args)
        else:
            loop.create_task(ntpsync_loop(method_args))
    elif m == "mailq":
        if DEBUG:
            mailq(method_args)
        else:
            loop.create_task(mailq_loop(method_args))
    elif m == "smartctl":
        if DEBUG:
            smartctl(method_args)
        else:
            loop.create_task(smartctl_loop(method_args))
    elif m == "mdadm":
        if DEBUG:
            mdadm(method_args)
        else:
            loop.create_task(mdadm_loop(method_args))
    elif m == "apt":
        if DEBUG:
            apt(method_args)
        else:
            loop.create_task(apt_loop(method_args))
    elif m == "filter_check":
        if DEBUG:
            filter_check(method_args)
        else:
            loop.create_task(filter_check_loop(method_args))
    elif m == "mistert_audit_relayers":
        if DEBUG:
            mistert_audit_relayers(method_args)
        else:
            loop.create_task(mistert_audit_relayers_loop(method_args))
    elif m == "oomkiller":
        if DEBUG:
            oomkiller(method_args)
        else:
            loop.create_task(oomkiller_loop(method_args))
    elif m == "mysql_max_connections":
        if DEBUG:
            mysql_max_connections(method_args)
        else:
            loop.create_task(mysql_max_connections_loop(method_args))

if not DEBUG:
    loop.run_forever()
